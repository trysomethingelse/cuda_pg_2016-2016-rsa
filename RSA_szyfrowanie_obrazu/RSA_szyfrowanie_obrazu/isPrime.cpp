#include "isPrime.h"
bool isPrimeCPU(const uint64_t number) //zwraca 1 gdy pierwsza
{
	if (number % 2 == 0) return 0;
	for (uint64_t i = 3; i*i <= number; i += 2)
	{
		if (number % i == 0)
		{
			return 0;
		}
	}
	return 1;
}