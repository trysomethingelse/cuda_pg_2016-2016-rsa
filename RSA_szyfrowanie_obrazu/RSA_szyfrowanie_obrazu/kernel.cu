
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include "generateKeys.h"
#include "pow_mod.h"


#include <stdio.h>
#include <stdint.h>
#include <iostream>
#include <thrust\host_vector.h>
#include <cmath>

using namespace std;


cudaError_t addWithCuda(int *c, const int *a, const int *b, unsigned int size);
thrust::host_vector<bool> decToBinaryVector(int dec);
void showVectorBinary(thrust::host_vector<bool> vector);
void showVectorDecimal(thrust::host_vector<bool> vector);

__global__ void addKernel(int *c, const int *a, const int *b)
{
    int i = threadIdx.x;
    c[i] = a[i] + b[i];
}

int main()
{
//	uint64_t n = 0, e = 0, d = 0, encrypted = 0, decrypted = 0 ;

	char key = 't';
	srand(time(NULL));
	while (key != 'n')
	{
		/*
		cout << "GENEROWANIE" << endl;
		generateKeysCPU(&n, &e,&d);

		cout << "Wygenerowane klucze: " << endl <<"n = "<<n<<", publiczny = "<<e<<", prywatny = "<<d<< endl;
		//encrypted = pow_modFast(713, 59, 43);
		encrypted = pow_modFast(n,e, 41);
		cout << "Zaszyfrowana liczba 41: " << encrypted << endl;
		//decrypted = pow_modFast(713, 179, encrypted);
		decrypted = pow_modFast(n,d, encrypted);
		cout << "Odszyfrowana liczba to: " << decrypted << endl;*/
		//number = generateOdd();
		//cout << "Wygenerowana:" << endl;

		int sizeInt = 8 * sizeof(int);
		//sizeInt = 1024;
		thrust::host_vector<bool>number1(sizeInt), number2(sizeInt), out(sizeInt), rest(sizeInt),outVDec(sizeInt),number3(sizeInt);
		int error = 0;
		int in1 = 2313221;
		int in2 = 1;
		int in3 = 12456;
		int r = 0;
		int nbTests = 1000;
		
		
		number1 = decToBinaryVector(in1);
		number2 = decToBinaryVector(in2);
		number3 = decToBinaryVector(in3);
		rest = decToBinaryVector(r);
		
		//number1 = generateOdd();
		//number2 = generateOdd();

		
		//out = substractVectors(number1, number2, &error);
		number1 = deleteStartZeros(number1);
		number2 = deleteStartZeros(number2);
		bool test = 0;
		test = millerRabinTest(number1,nbTests,number1.size());
		//out = addVectors(number1, number2);
		divisionVectors(number1, number3, &rest);

		showVectorBinary(number1);
		showVectorBinary(number2);
		cout << "Wynik bin: ";
		showVectorBinary(out);
	
		
		showVectorDecimal(number1);
		showVectorDecimal(number2);
		if (test = 1)
			cout << "liczba: " << in1 << " przeszla " << nbTests << " testow" << endl;
		else
			cout << "liczba: " << in1 << " NIE przeszla " << nbTests << " testow" << endl;


		cout << "Wynik: ";
		showVectorDecimal(out);
		cout << "Reszta: ";
		showVectorDecimal(rest);
		

		cout <<endl<< "kontynuowac t/n? " << endl;
		cin >> key;
	}
	std::system("PAUSE");
    return 0;
}

void showVectorBinary(thrust::host_vector<bool> vector)
{
	for (int i = vector.size() - 1; i >= 0; i--)
	{
		cout << vector[i];
	}
	cout << endl;
}
void showVectorDecimal(thrust::host_vector<bool> vector)
{
	int out = 0;
	for (int i = vector.size() - 1; i >= 0; i--)
	{
		out += vector[i] * pow(2,i);
	}
	cout << out;
	cout << endl;
}



thrust::host_vector<bool> decToBinaryVector(int dec)
{
	thrust::host_vector<bool>out(8*sizeof(int));
	int slide = 1;


	for (int i = 0; i < 8* sizeof(int); i++)
	{
		out[i] = (dec & slide)>>i ;//wybieranie kolejnego bitu
		slide = slide << 1;
	}
	return out;
}

cudaError_t addWithCuda(int *c, const int *a, const int *b, unsigned int size)
{
    int *dev_a = 0;
    int *dev_b = 0;
    int *dev_c = 0;
    cudaError_t cudaStatus;

    // Choose which GPU to run on, change this on a multi-GPU system.
    cudaStatus = cudaSetDevice(0);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
        goto Error;
    }

    // Allocate GPU buffers for three vectors (two input, one output)    .
    cudaStatus = cudaMalloc((void**)&dev_c, size * sizeof(int));
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMalloc failed!");
        goto Error;
    }

    cudaStatus = cudaMalloc((void**)&dev_a, size * sizeof(int));
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMalloc failed!");
        goto Error;
    }

    cudaStatus = cudaMalloc((void**)&dev_b, size * sizeof(int));
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMalloc failed!");
        goto Error;
    }

    // Copy input vectors from host memory to GPU buffers.
    cudaStatus = cudaMemcpy(dev_a, a, size * sizeof(int), cudaMemcpyHostToDevice);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMemcpy failed!");
        goto Error;
    }

    cudaStatus = cudaMemcpy(dev_b, b, size * sizeof(int), cudaMemcpyHostToDevice);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMemcpy failed!");
        goto Error;
    }

    // Launch a kernel on the GPU with one thread for each element.
    addKernel<<<1, size>>>(dev_c, dev_a, dev_b);

    // Check for any errors launching the kernel
    cudaStatus = cudaGetLastError();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "addKernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
        goto Error;
    }
    
    // cudaDeviceSynchronize waits for the kernel to finish, and returns
    // any errors encountered during the launch.
    cudaStatus = cudaDeviceSynchronize();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching addKernel!\n", cudaStatus);
        goto Error;
    }

    // Copy output vector from GPU buffer to host memory.
    cudaStatus = cudaMemcpy(c, dev_c, size * sizeof(int), cudaMemcpyDeviceToHost);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMemcpy failed!");
        goto Error;
    }

Error:
    cudaFree(dev_c);
    cudaFree(dev_a);
    cudaFree(dev_b);
    
    return cudaStatus;
}
