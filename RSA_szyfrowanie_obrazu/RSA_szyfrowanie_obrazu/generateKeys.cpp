#include "generateKeys.h"


uint64_t nwd(uint64_t, uint64_t);
uint64_t privateKey(uint64_t, uint64_t);
uint64_t nwdBetter(uint64_t a, uint64_t b);
bool randomBit();

bool generateKeysCPU(uint64_t* nNumber, uint64_t *publicNumber, uint64_t *privateNumber)// 0 powodzenie
{

	uint32_t p=0, q=0; //liczby pierwsze
	uint64_t n=0 , eulerFunc = 0,
			 e=0,//wykladnik publiczny
			 d = 0;//wykladnik prywatny
	
	srand(time(NULL));
	p = rand() + (rand() << 15) + ((rand() % 4) << 30); //rand() generuje liczb� tylko na 15 bitach
	p %= 10000;
	while (!isPrimeCPU(p))
		p--;
	q = p - rand()%1000;

	while (!isPrimeCPU(q))
		q++;

	n = (uint64_t)(p*q);
	eulerFunc = (uint64_t)((p - 1)*(q - 1));
	
	e = (rand() + ((rand()&1) << 15)) % eulerFunc; //mala liczba e wzglednie pierwsza do eulerFunc i mniejsza od eulerFunc

	while (e != 1 && nwd(e, eulerFunc) != 1)//dopoki nie jest liczba wzglednie pierwsza
		e--;
	
	d = privateKey(e, eulerFunc);
	
	/*
	n = 26167;
	e = 191;
	d = */

	*nNumber = n;
	*publicNumber = e;
	*privateNumber = d;

	return 0;
}
uint64_t privateKey(uint64_t n, uint64_t eulerFunc)
{
	for (uint64_t i = 2; i < eulerFunc; i+=1)
	{
		if ((i*n) % eulerFunc == 1)
		{
			return i;
		}

	}
	return 0;
}
uint64_t nwd(uint64_t a, uint64_t b)
{
	uint64_t tempA = a, tempB = b;
	while (tempA != tempB)
	{
		if (tempA < tempB)
		{
			tempB = tempB - tempA;
		}
		else
			tempA = tempA - tempB;
	}
	return tempA;
}
uint64_t nwdBetter(uint64_t a, uint64_t b)
{
	uint64_t tempA = a, tempB = b, rest = 0;
	while (1)
	{
		rest = tempA % tempB;
		if (rest == 0) return  tempB;
		else
		{
			tempA = tempB;
			tempB = rest;
		}
	}
}
host_vector<bool> generateOdd(int size) //generuje liczbe nieparzysta przechowywana w tablicy bitowo
{
	host_vector<bool> number(size);

	number[0] = 1;//by byla nieparzysta
	number[size - 1] = 1; //by byla maksymalnie duza

	for (int i = 1; i < size-1 ; i++)
	{
		number[i] = randomBit();
	}
	
	return number;
}
host_vector<bool> generateRandom(int size) //generuje liczbe 
{
	host_vector<bool> number(size);

	for (int i = 0; i < size; i++)
	{
		number[i] = randomBit();
	}
	return number;
}

bool randomBit()
{
	return rand() % 2;
}
bool millerRabinTest(host_vector<bool> n, int numberOfTests,int size)
{
	host_vector <bool>  s(1);//licznik wystapien dzielen przez 2
	s[0] = 0;
	int sizeOfN = n.size();
	int error = 0;

	host_vector <bool> d(sizeOfN); 
	host_vector <bool> nb0(1); nb0[0] = 0; //liczba 0 w wektorze
	host_vector <bool> nb1(1); nb1[0] = 1;//1
	host_vector <bool> nb2(2); nb2[0] = 0; nb2[1] = 1; //2

	
	host_vector <bool> rest(sizeOfN);

	d = n;
	d[0] = 0;//odejmuje 1 od d

	do{
		d = divisionVectors(d, nb2, &rest);
		s = addVectors(s, nb1);
	} while (isGreaterOrEqual(d,nb0) != 0 && d[0] == 0);//oblicza ile jest dzielnik�w 2^x dopoki nie ma 0 i kolejna liczba parzysta


	host_vector <bool> nMinus1 = substractVectors(n, nb1, &error);
	host_vector <bool> nMinus2 = substractVectors(n, nb2, &error);
	host_vector <bool> xMR(sizeOfN);//wyraz ciagu Millera Rabina

	host_vector <bool> a(sizeOfN); //a ->wylosowana liczba od 2 do number-2
	for (int i = 0; i < numberOfTests; i++)
	{
		do {
			a = generateOdd(size);
		} while (isGreaterOrEqual(a, nb1) == 0 || isGreaterOrEqual(a, nMinus1) == 0); //dopoki a nie jest wieksza od 2 i dopoki a nie  mniejsza od n-2
		xMR = powModuloVectors(a, d, n);
		if (isGreaterOrEqual(xMR, nb1) == 0 || isGreaterOrEqual(xMR, nMinus1) == 0)continue;//nastepny obieg
		
		host_vector <bool> j(1);
		j = nb1;

		while (isGreaterOrEqual(j, s) == -1 && isGreaterOrEqual(xMR, nMinus1) != 0)
		{
			xMR = powModuloVectors(xMR, nb2, n);
			if (isGreaterOrEqual(xMR, nb1) == 0)return 0;//jesli r�wna 1 to n nie przesz�a test�w
			j = addVectors(j, nb1);
		}
		if (isGreaterOrEqual(xMR, nMinus1) != 0)return 0;//przedostani wyraz musi by� r�wny n-1
		
	}
	return 1;//przesz�a testy


}
host_vector<bool> substractVectors(host_vector<bool> number1, host_vector<bool> number2, int *error)//odejmowanie number2 od number1 gdy  n2>n1 zwraca n1
{
	//kody bledow
	//0 - rowne
	//-1 - odjemnik wiekszy od odjemnej - zwraca odjemna
	//1 - dzialanie poprawne
	host_vector<bool> copy1 = deleteStartZeros(number1);
	host_vector<bool> copy2 = deleteStartZeros(number2);

	int size1 = copy1.size();
	int size2 = copy2.size();
	host_vector <bool> out(size1);
	out = copy1;
	int borrow = 0;
	*error = 1;
	
	int check = isGreaterOrEqual(copy1, copy2);
 	if (check == -1)
	{
		*error = -1;
		return copy1;
	}
	else if (check == 0)
	{
		*error = 0;
		thrust::fill(out.begin(), out.end(), 0); //zwraca 0
		return out;
	}

	for (int i = 0; i < size2; i++)
	{
		if (out[i] == copy2[i])
			out[i] = 0;
		else if (copy2[i])//jesli odjemnik ma 1 na danym miejscu
		{
			do
			borrow++;
			while (!out[i + borrow]); //szukaj miejsca do pozyczenia
	
		
				out[i + borrow] = 0;
				while (borrow != 0)//cofaj sie w pozycji
				{
					borrow--;
					out[i + borrow] = 1;//zaminiaj wczesniejsza pozyczke na 1
				}
			}
		
		//jesli odjemnik ma 0 na danym miejscu to pozostawiamy wyjscie bez zmian
	}
	return out;
}
int isGreaterOrEqual(host_vector<bool>number1, host_vector<bool>number2)//- 1 number2 wi�ksza, 0 - r�wne, 1-number1 wi�ksza
{
	int size1 = number1.size();
	int size2 = number2.size();

	if (size2 > size1)
		return -1;
	else if (size2<size1)
		return 1;

	for (int i = size2-1; i >= 0; i-- )
	{
		if (number1[i] > number2[i])
			return 1;//number1 wiekszy
		else if (number1[i] < number2[i])
			return -1;//number2 wiekszy
	}
	return 0;//r�wne
}
host_vector<bool> powModuloVectors(host_vector<bool> base, host_vector<bool> pow, host_vector<bool> mod)
{
	int sizeMod = mod.size();
	int sizePow = pow.size();
	int sizeBase = base.size();
	int error = 0;

	host_vector<bool>rest(2*sizeMod);
	host_vector<bool>out(2*sizeMod);
	out[0] = 1;

	divisionVectors(base, mod, &rest);//dla potegi 1
	for (int i = 0; i < sizePow; i++)
	{
		if (pow[i] == 1)//gdy dany bit jest 1
		{
			out = multipyVectors(out, rest);
			divisionVectors(out, mod, &out);
		}
		rest = multipyVectors(rest, rest);
		divisionVectors(rest, mod, &rest);

	}
	return deleteStartZeros(out);
}
host_vector<bool> divisionVectors(host_vector<bool> n, host_vector<bool> d, host_vector<bool>*rest)
{
	host_vector<bool> copyN = deleteStartZeros(n);
	host_vector<bool> copyD = deleteStartZeros(d);

	int sizeN = copyN.size();
	int sizeD = copyD.size();
	int m = sizeN - sizeD;
	int error = 0;
	if (m < 0)//d>n
	{
		host_vector<bool> out(1);
		out[0] = 0;
		*rest = n;
		return out;
	}


	host_vector<bool>out(m+1);
	host_vector<bool>restTemp(sizeN);
	host_vector<bool>dSlided(sizeD);

	while (m != -1)
	{
		dSlided = slideVector(copyD, m, m);

		restTemp = substractVectors(copyN, dSlided, &error);

		restTemp = deleteStartZeros(restTemp);


		if (error == 0)//jesli rowne
		{
			out[m] = 1;
			break;
		}
		else if (error == -1)//jesli odjemna wieksza
			out[m] = 0;
		else if (error == 1)//jesli ok
			out[m] = 1;
		m--;
		copyN = restTemp;
		
	}
	out = deleteStartZeros(out);

	*rest = restTemp;
	return out;
	
}
host_vector<bool> slideVector(host_vector<bool> in, int n,int add0) //w prawo n<0 , w prawo wystepuja bledy
{
	int size = in.size() + add0;
	host_vector<bool> out(size);
	thrust::fill(out.begin(), out.end(), 0);
	
	for (int i = 0; i < in.size(); i++)
	{
		if (n >= 0)//w lewo
			out[(i + n) % size] = in[i];
		else // w prawo
			out[i] = in[(i + abs(n)) % in.size()];
	}
	return out;
}
host_vector<bool> deleteStartZeros(host_vector<bool> in)
{
	int size = in.size();
	host_vector <bool> out = in;
	for (int i = size - 1; i>= 0 && out[i] == 0 && size >= 0; i--)
		out.resize(i);
	return out;
}
host_vector<bool> addVectors(host_vector<bool> n, host_vector<bool> m)
{
	int sizeN = n.size();
	int sizeM = m.size();
	int sizeOut = sizeN;
	host_vector<bool> out(sizeN) ;
	out = n;

	int transfer = 0;//przechowuje przeniesienia

	for (int i = 0; transfer!= 0 || i < sizeM; i++)//dopoki jest cos w przeniesieniu lub nie dodano ca�osci
	{
		if (i > sizeOut - 1)//jesli wychodzi poza wielkosc 
		{
			sizeOut += 1;
			out.resize(sizeOut);
			out[i] = 0;
		}
		if (i < sizeM)
		{
			if (out[i] == 1 && m[i] == 1)
			{
				if (transfer)out[i] = 1;
				else
				{
					transfer++; out[i] = 0;
				}
			}
			else if (out[i] != m[i])
			{
				if (transfer) out[i] = 0;
				else  out[i] = 1;
			}
			else if (out[i] == 0 && m[i] == 0)
				if (transfer){ out[i] = 1; transfer--; }

		}
		else
		{
			if (out[i] == 1)
				out[i] = 0;
			else{
				out[i] = 1;
				transfer--;
			}
		}
	}
	return out;
}
host_vector<bool> multipyVectors(host_vector<bool> n, host_vector<bool> m)
{
	int sizeN = n.size();
	int sizeM = m.size();
	int sizeOut = sizeN;
	host_vector<bool> out(sizeN);
	thrust::fill(out.begin(), out.end(), 0);
	
	for (int i = 0; i < sizeM; i++)
	{
		if (m[i])
		{
			out = addVectors(out, slideVector(n,i,i));
		}
	}
	return out;
}