#include "pow_mod.h"

uint64_t pow_mod(uint64_t mod, uint64_t power, uint64_t message)
{
	uint64_t rest = 1;
	for (uint64_t i = 1; i <= power; i++)
	{
		rest = (rest*message) % mod;
	}
	return rest;
}
uint64_t pow_modFast(uint64_t mod, uint64_t power, uint64_t message)
{
	uint64_t powerTemp = power;
	uint64_t rest = 0, out = 1;
	
	rest = message % mod; //reszta z dzielenia dla potegi 1
	
	
	for (int i = 0; i < 8*sizeof(uint64_t); i++)//przeskakuje po wszystkich bitach potegi
	{
		if (powerTemp & 1 == 1)//gdy pierwszy bit wynosi 1
		{		
			out *= rest;
			if (out > mod) out %= mod;
		}
		powerTemp = powerTemp >> 1;//kolejny bit po prawej
		rest *= rest;
		if (rest > mod)rest %= mod;
	}
	return out;
}
