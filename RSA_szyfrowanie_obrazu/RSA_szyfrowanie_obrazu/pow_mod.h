#include <stdint.h>

uint64_t pow_mod(uint64_t n, uint64_t e, uint64_t message);
uint64_t pow_modFast(uint64_t mod, uint64_t power, uint64_t message);