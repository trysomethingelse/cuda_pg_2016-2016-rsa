#include <stdint.h>
#include <iostream>
#include <cstdlib>
#include <time.h>
#include <thrust/host_vector.h>
#include "isPrime.h"


using namespace std;
using namespace thrust;
#define PRIME_SIZE 1024

bool generateKeysCPU(uint64_t *, uint64_t*, uint64_t*); //0 gdy wygenerowano poprawnie
host_vector<bool> generateOdd(int size); //generuje liczbe nieparzysta
bool millerRabinTest(host_vector<bool> number, int numberOfTests,int size);

host_vector<bool> substractVectors(host_vector<bool> number1, host_vector<bool> number2, int* error);
int isGreaterOrEqual(host_vector<bool>number1, host_vector<bool>number2);

host_vector<bool> moduloVectors(host_vector<bool> base, host_vector<bool> mod, int *errorOut);

host_vector<bool> slideVector(host_vector<bool> in, int n, int add0); //w prawo n<0

host_vector<bool> divisionVectors(host_vector<bool> n, host_vector<bool> d, host_vector<bool>*rest);
host_vector<bool> deleteStartZeros(host_vector<bool> in);//usuwa zera z przodu wektora
host_vector<bool> addVectors(host_vector<bool> n, host_vector<bool> m);
host_vector<bool> multipyVectors(host_vector<bool> n, host_vector<bool> m);
host_vector<bool> powModuloVectors(host_vector<bool> base, host_vector<bool> pow, host_vector<bool> mod);
host_vector<bool> generateRandom(int size); //generuje liczbe 




//host_vector<bool> substract(host_vector<bool> number, int number2);